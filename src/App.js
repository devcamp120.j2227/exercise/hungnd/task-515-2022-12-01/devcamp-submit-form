//Import thư viện bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Form from './component/Form';
import Tittle from './component/Tittle';

function App() {
  return (
    <div className='container mt-5'>
      <Tittle/>
      <Form/>
    </div>
  );
}
export default App;
