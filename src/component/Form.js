import { Component } from "react";

class Form extends Component {
    onChangeInpFirstName(event) {
        console.log(event.target.value);
    }
    onChangeInpLastName(event) {
        console.log(event.target.value);
    }
    onChangeSelect(event) {
        console.log(event.target.value);
    }
    onChangeSubject(event) {
        console.log(event.target.value);
    }
    onSubmitButton() {
        console.log("Form đã được submit");
    }
    render() {
        return(
            <>
                <div className="div-form">
                    <div className="row mt-3">
                        <div className="col-3">
                            <label>First Name</label>
                        </div>
                        <div className="col-9">
                            <input className="form-control" placeholder="Your name..." onClick={this.onChangeInpFirstName}></input>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-3">
                            <label>Last Name</label>
                        </div>
                        <div className="col-9">
                            <input className="form-control" placeholder="Your last name..." onClick={this.onChangeInpLastName}></input>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-3">
                            <label>Country</label>
                        </div>
                        <div className="col-9">
                            <select className="form-control" onChange={this.onChangeSelect}>
                                <option>Australia</option>
                                <option>Mỹ</option>
                            </select>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-3">
                            <label>Subject</label>
                        </div>
                        <div className="col-9">
                            <textarea className="form-control" rows={9} placeholder="Write something..." onClick={this.onChangeSubject}></textarea>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <button type="submit" className="btn-send" onClick={this.onSubmitButton}>Send data</button>
                    </div>
                </div>
            </>
        )
    }
}
export default Form;